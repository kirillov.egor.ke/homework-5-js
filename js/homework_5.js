// Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:
//
//    1. При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
//    2. Создать метод getAge() который будет возвращать сколько пользователю лет.
//    3. Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре,
//    соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
//
//
// Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.

// Теория. Экранирование нужно для того, чтобы спецсимволы, которые используются в написании кода,
// можно было вывести (показать) пользователю. Если их не экранировать, то в коде будет ошибка

function createNewUser() {
    const userFirstName = prompt('Как тебя зовут?');
    const userLastName = prompt('Какая у тебя фамилия?');
    const birthday = prompt('Введи дату рождения в формате dd.mm.yyyy');
    const newUser = {
        firstName: userFirstName,
        lastName: userLastName,
        getLogin: function () {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        getAge: function () {
            // Скажу честно, это решение я нашёл в инете (и при этом так и не понял, как это работает)
            const d = birthday.split('.');
            if( typeof d[2] !== "undefined" ) {
                date = d[2]+'.'+d[1]+'.'+d[0];
                return ((new Date().getTime() - new Date(date)) / (24 * 3600 * 365.25 * 1000)) | 0;
            }
            return 0;
        },
        getPassword: function () {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + birthday.slice(-4);
        }
    }
    return newUser;
}

const user = createNewUser();

console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());